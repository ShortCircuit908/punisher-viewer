<?php
/**
 * @author Caleb Milligan
 * Created 3/21/2018
 */

$result = ["success" => false];
try {
	require_once "NameResolver.php";
	require_once "PunisherPDO.php";
	require_once "PunisherConstants.php";
	$db = new PunisherPDO(
			PunisherConstants::DB_HOST,
			PunisherConstants::DB_PORT,
			PunisherConstants::DB_USERNAME,
			PunisherConstants::DB_PASSWORD,
			PunisherConstants::DB_NAME
	);
	$page = 0;
	$uuids = null;
	if (isset($_GET["p"])) {
		$page = $_GET["p"];
	}
	if (isset($_GET["player"])) {
		$uuids = NameResolver::resolveUuid($_GET["player"]);
	}
	$punishments = $db->fetchPunishments($uuids, $page, PunisherConstants::RESULTS_PER_PAGE);
	foreach ($punishments as $index => $punishment) {
		$punishments[$index]["target_name"] = NameResolver::resolveUsername($punishment["target_id"]);
		$punishments[$index]["issuer_name"] = $punishment["issuer_id"] ? NameResolver::resolveUsername($punishment["issuer_id"]) : "{CONSOLE}";
		$punishments[$index]["server"] = $punishment["server"] ? $punishment["server"] : "{GLOBAL}";
	}
	$result["success"] = true;
	$result["punishments"] = $punishments;
	$result["pages"] = $db->fetchPageCount($uuids, PunisherConstants::RESULTS_PER_PAGE);
	$result["page"] = isset($_GET["p"]) ? $_GET["p"] + 0 : 0;
}
catch (Exception $e) {
	error_log($e);
	$result["error"] = $e;
}
http_response_code(200);
header("Content-type: application/json");
print(json_encode($result));
