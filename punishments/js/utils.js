function durationToSeconds(duration) {
	var pattern = /([-+]?)P(?:([-+]?[0-9]+)D)?(T(?:([-+]?[0-9]+)H)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)(?:[.,]([0-9]{0,9}))?S)?)?/i;
	var result = pattern.exec(duration);
	if (result) {
		if (result[3] !== "T") {
			var negate = result[1] === "-";
			var dayMatch = result[2] || 0;
			var hourMatch = result[4] || 0;
			var minuteMatch = result[5] || 0;
			var secondMatch = result[6] || 0;
			var fractionMatch = result[7];
			if (dayMatch || hourMatch || minuteMatch || secondMatch) {
				var daysAsSecs = dayMatch * 24 * 60 * 60;
				var hoursAsSecs = hourMatch * 60 * 60;
				var minsAsSecs = minuteMatch * 60;
				var seconds = secondMatch * 1;
				return (daysAsSecs + hoursAsSecs + minsAsSecs + seconds) * (negate ? -1 : 1);
			}
		}
	}
	return null;
}

function formatSeconds(seconds) {
	if (seconds === null || seconds === undefined) {
		return "";
	}
	var builder = "";
	if (seconds < 0) {
		seconds *= -1;
		builder = "-";
	}
	var years = Math.floor(seconds / (60 * 60 * 24 * 365));
	seconds -= years * (60 * 60 * 24 * 365);
	var days = Math.floor(seconds / (60 * 60 * 24));
	seconds -= days * (60 * 60 * 24);
	var hours = Math.floor(seconds / (60 * 60));
	seconds -= hours * (60 * 60);
	var minutes = Math.floor(seconds / 60);
	seconds -= minutes * 60;
	var weeks = Math.floor(days / 7);
	days -= weeks * 7;
	if (years > 0) {
		builder += years + "y ";
	}
	if (weeks > 0) {
		builder += weeks + "w ";
	}
	if (days > 0) {
		builder += days + "d ";
	}
	if (hours > 0) {
		builder += hours + "h ";
	}
	if (minutes > 0) {
		builder += minutes + "m ";
	}
	if (seconds > 0 || builder.length === 0) {
		builder += seconds + "s";
	}
	return builder.toString().trim();
}

/**
 *
 * @param url
 * @param name
 * @returns {string|null}
 */
function getUrlParameterByName(url, name) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
	var results = regex.exec(url);
	if (!results) {
		return null;
	}
	if (!results[2]) {
		return "";
	}
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}
