$(document).ready(function () {
	$("#input-submit").on("click", function () {
		submit();
	});
	$("#control-prev").on("click", function () {
		var page = (~~$("#page-indicator").html() || 0) - 1;
		submit(page);
	});
	$("#control-next").on("click", function () {
		var page = (~~$("#page-indicator").html() || 0) + 1;
		submit(page);
	});
	window.onpopstate = function (event) {
		if (event.state) {
			submit(event.state.p, event.state.player, true);
		}
	};
	submit(new URLSearchParams(window.location.search).get("p") || 0);
});

function submit(page, player, nohistory) {
	if (page === null || page === undefined || typeof page !== "number") {
		page = 0;
	}
	if (player === null || player === undefined || typeof player !== "string") {
		player = $("#input-username").val() || "";
	}
	if (!nohistory) {
		window.history.pushState(
			{
				p: page,
				player: player
			},
			document.title,
			//window.location.href.split(/[?#]/)[0]
			window.location.href.split(/[?#]/)[0] + "?" + (player === "" ? "" : "player=" + encodeURIComponent(player) + "&") + "p=" + page
		);
	}
	$.ajax({
		url: "punishments.php",
		method: "GET",
		data: {
			p: page,
			player: player
		}
	}).done(function (data, status, jqxhr) {
		if (data.success) {
			var show_prev = data.pages > 0 && data.page > 0;
			var show_next = data.pages > 0 && data.page < data.pages - 1;
			$("#control-prev").css({
				visibility: show_prev ? "visible" : "hidden"
			});
			$("#control-next").css({
				visibility: show_next ? "visible" : "hidden"
			});
			$("#page-indicator").html(data.page);
			var inner_html = "";
			data.punishments.forEach(function (punishment) {
				var date = new Date(punishment.issued).toLocaleString();
				var duration = formatSeconds(durationToSeconds(punishment.duration));
				inner_html += "<tr><td><a href='details/?id=" + punishment.id + "' target='_blank'>" + punishment.id
					+ "</a></td><td><a href='./?player=" + punishment.target_name + "' target='_blank'>" + punishment.target_name
					+ "</a></td><td>" + punishment.server + "</td><td>" + punishment.issuer_name + "</td><td>" + date + "</td><td>"
					+ punishment.action + "</td><td>" + duration + "</td><td>" + (punishment.acknowledged ? "Yes" : "No")
					+ "</td><td>" + (punishment.reason || "") + "</td></tr>";
			});
			var log = $("#punishment-log");
			log.empty();
			log.append(inner_html);
		}
	});
}
