<?php
/**
 * @author Caleb Milligan
 * Created 3/21/2018
 */
include_once "PunisherPDO.php";
include_once "PunisherConstants.php";

class NameResolver {
	private static $db = null;
	
	private static function ensureDB() {
		if (!self::$db) {
			self::$db = new PunisherPDO(PunisherConstants::DB_HOST, PunisherConstants::DB_PORT, PunisherConstants::DB_USERNAME, PunisherConstants::DB_PASSWORD, PunisherConstants::DB_NAME);
		}
	}
	
	public static function resolveUsername($uuid) {
		self::ensureDB();
		$stmt = self::$db->prepare("SELECT `username` FROM `punisher_uuid_map` WHERE LOWER(`id`)=LOWER(:uuid)");
		$uuid = preg_replace("/([0-9a-f]{8})-?([0-9a-f]{4})-?([0-9a-f]{4})-?([0-9a-f]{4})-?([0-9a-f]{12})/i", "$1-$2-$3-$4-$5", $uuid);
		$stmt->bindParam(":uuid", $uuid, PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (empty($result)) {
			/*
			$username = self::fetchUsername($uuid);
			if ($username) {
				$stmt = self::$db->prepare("REPLACE INTO `punisher_uuid_map` (`id`, `username`) VALUES (:uuid, :username)");
				$stmt->bindParam(":uuid", $uuid);
				$stmt->bindParam(":username", $username);
				$stmt->execute();
			}
			return $username;
			*/
			return str_replace("-", "", $uuid);
		}
		else {
			return $result[0]["username"];
		}
	}
	
	public static function resolveUuid($username) {
		self::ensureDB();
		$stmt = self::$db->prepare("SELECT `id` FROM `punisher_uuid_map` WHERE LOWER(`username`) LIKE CONCAT('%', LOWER(:username), '%')");
		$stmt->bindParam(":username", $username, PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$val = [];
		foreach ($result as $index => $value) {
			$val[] = $value["id"];
		}
		return $val;
	}
	
	public static function fetchUsername($uuid) {
		$uuid = str_replace("-", "", $uuid);
		$contents = file_get_contents("https://api.mojang.com/user/profiles/$uuid/names");
		var_dump($contents);
		$parsed = json_decode($contents, true);
		$name = null;
		$latest = 0;
		foreach ($parsed as $name_info) {
			if (!isset($name_info["changedToAt"])) {
				if ($latest == 0) {
					$name = $name_info["name"];
				}
				continue;
			}
			if ($latest < $name_info["changedToAt"]) {
				$latest = $name_info["changedToAt"];
				$name = $name_info["name"];
			}
		}
		return $name;
	}
}
