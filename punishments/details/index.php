<?php
/**
 * @author Caleb Milligan
 * Created 4/10/2018
 */
$id = null;
if (isset($_GET["id"])) {
	if (filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false) {
		$id = $_GET["id"];
	}
}
else {
	foreach ($_GET as $key => $value) {
		if (filter_var($key, FILTER_VALIDATE_INT) !== false) {
			$id = $key;
			break;
		}
	}
}

if ($id === null) {
	http_response_code(404);
	die();
}

$punishment = null;
try {
	require_once "../NameResolver.php";
	require_once "../PunisherPDO.php";
	require_once "../PunisherConstants.php";
	$db = new PunisherPDO(
			PunisherConstants::DB_HOST,
			PunisherConstants::DB_PORT,
			PunisherConstants::DB_USERNAME,
			PunisherConstants::DB_PASSWORD,
			PunisherConstants::DB_NAME
	);
	$punishment = $db->fetchPunishment($id);
	if (!$punishment) {
		http_response_code(404);
		die();
	}
	$punishment["target_name"] = NameResolver::resolveUsername($punishment["target_id"]);
	$punishment["issuer_name"] = $punishment["issuer_id"] ? NameResolver::resolveUsername($punishment["issuer_id"]) : "{CONSOLE}";
	$punishment["server"] = $punishment["server"] ? $punishment["server"] : "{GLOBAL}";
}
catch (Exception $e) {
	error_log($e);
	trigger_error("A problem occurred while querying the database");
	die();
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="author" content="ShortCircuit908">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Golden Sands Punishments</title>
		<link rel="stylesheet" type="text/css"
			  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
			  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
			  crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Raleway:400|Ubuntu:700" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../css/index.css">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
				integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
				crossorigin="anonymous"></script>
		<script src="../js/utils.js" type="application/javascript"></script>
		<style>
			#container-display {
				overflow-x: initial;
			}
		</style>
	</head>
	<body>
		<div id="main-container">
			<div class="my-container" id="container-display">
				<table>
					<thead>
						<tr>
							<th>Player</th>
							<th>Server</th>
							<th>Issued by</th>
							<th>Timestamp</th>
							<th>Action</th>
							<th>Duration</th>
							<th>Ack</th>
							<th>Reason</th>
						</tr>
					</thead>
					<tbody id="punishment-log">
						<tr>
							<td>
								<a href="../?player=<?php print($punishment["target_name"]); ?>"
								   target="_blank"><?php print($punishment["target_name"]); ?></a>
							</td>
							<td><?php print($punishment["server"]); ?></td>
							<td><?php print($punishment["issuer_name"]); ?></td>
							<td id="column-issued"><?php print($punishment["issued"]); ?></td>
							<td><?php print($punishment["action"]); ?></td>
							<td id="column-duration"><?php print($punishment["duration"]); ?></td>
							<td><?php print($punishment["acknowledged"] ? "Yes" : "No") ?></td>
							<td><?php print($punishment["reason"]); ?></td>
							<script>
								var column_issued = $("#column-issued");
								var column_duration = $("#column-duration");
								var date = new Date(column_issued.html()).toLocaleString();
								var duration = formatSeconds(durationToSeconds(column_duration.html()));
								column_issued.html(date);
								column_duration.html(duration);
							</script>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>
