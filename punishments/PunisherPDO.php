<?php
/**
 * @author Caleb Milligan
 * Created 3/21/2018
 */
include_once "PunisherConstants.php";

class PunisherPDO extends PDO {
	public function __construct($host, $port, $username, $password, $database) {
		parent::__construct("mysql:host=$host;port=$port;dbname=$database;", $username, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	}
	
	/**
	 * @param string|array [$uuids]
	 * @param int [$results_per_page]
	 * @return int
	 * @throws PDOException
	 */
	public function fetchPageCount($uuids = null, $results_per_page = PunisherConstants::RESULTS_PER_PAGE) {
		$query = "SELECT COUNT(*) FROM `punisher_punishments`";
		if ($uuids !== null) {
			if (!is_array($uuids)) {
				$uuids = [$uuids];
			}
			if (empty($uuids)) {
				return 0;
			}
			$query = "SELECT COUNT(*) FROM `punisher_punishments` WHERE LOWER(`target_id`) IN (" . implode(',', array_fill(0, count($uuids), '?')) . ")";
		}
		$stmt = self::prepare($query);
		if ($uuids) {
			foreach ($uuids as $i => $id) {
				$stmt->bindValue($i + 1, strtolower($id));
			}
		}
		$stmt->execute();
		return ceil($result = $stmt->fetchColumn() / $results_per_page);
	}
	
	/**
	 * @param string|array [$uuids]
	 * @param int [$page]
	 * @param int [$results_per_page]
	 * @return array
	 * @throws PDOException
	 */
	public function fetchPunishments($uuids = null, $page = 0, $results_per_page = PunisherConstants::RESULTS_PER_PAGE) {
		$query = "SELECT * FROM `punisher_punishments`";
		if ($uuids !== null) {
			if (!$uuids) {
				return [];
			}
			if (!is_array($uuids)) {
				$uuids = [$uuids];
			}
			$query .= " WHERE LOWER(`target_id`) IN (" . implode(',', array_fill(0, count($uuids), '?')) . ")";
		}
		$query .= " ORDER BY `issued` DESC";
		if ($results_per_page > 0 && $page >= 0) {
			$query .= " LIMIT ? OFFSET ?";
		}
		$stmt = self::prepare($query);
		$param_index = 1;
		if ($uuids) {
			foreach ($uuids as $j => $id) {
				$stmt->bindValue(($param_index++), strtolower($id));
			}
		}
		if ($results_per_page > 0 && $page >= 0) {
			$offset = $page * $results_per_page;
			$stmt->bindValue($param_index++, $results_per_page, PDO::PARAM_INT);
			$stmt->bindParam($param_index, $offset, PDO::PARAM_INT);
		}
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	/**
	 * @param int $id
	 * @return array
	 * @throws PDOException
	 */
	public function fetchPunishment($id) {
		$stmt = self::prepare("SELECT * FROM `punisher_punishments` WHERE `id`=:id");
		$stmt->bindParam(":id", $id);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (!$result) {
			return null;
		}
		return $result[0];
	}
}