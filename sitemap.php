<?php
/**
 * @author Caleb Milligan
 * Created 6/16/2018
 */
require_once "punishments/PunisherConstants.php";
require_once "punishments/PunisherPDO.php";

$db = new PunisherPDO(
		PunisherConstants::DB_HOST,
		PunisherConstants::DB_PORT,
		PunisherConstants::DB_USERNAME,
		PunisherConstants::DB_PASSWORD,
		PunisherConstants::DB_NAME
);
$stmt = $db->prepare("SELECT `id` FROM `punisher_punishments`");
$stmt->execute();
$ids = $stmt->fetchAll(PDO::FETCH_COLUMN);
header("Content-type: application/xml; charset=utf-8");
print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">"
		. "<url><loc>http://play.goldensandsmc.com/punishments/</loc><changefreq>hourly</changefreq><priority>0.8</priority></url>");
foreach ($ids as $id) {
	print("<url><loc>http://play.goldensandsmc.com/punishments/details/?id=$id</loc><changefreq>monthly</changefreq></url>");
}
print("</urlset>");
